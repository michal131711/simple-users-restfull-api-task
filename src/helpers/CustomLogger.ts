import { ConsoleLogger, Injectable, Logger, Res, Scope } from '@nestjs/common';
import { randomUUID } from 'crypto';
import { loggerMessage } from './functions';
import * as util from 'util';

@Injectable({ scope: Scope.TRANSIENT })
export class CustomLogger extends ConsoleLogger {
  private logger: Logger;

  constructor(log: Logger) {
    super();
    this.logger = log;
  }

  async sendDataResponse(
    @Res() response,
    statusCode: number,
    data: object,
  ): Promise<any> {
    const reqId = randomUUID();

    const prettyFormatCodeData = util.inspect(data, { depth: null });

    loggerMessage(
      this.logger,
      `Res - requestId: ${reqId}, code: ${statusCode}, data: ${prettyFormatCodeData}`,
    );

    return await response.status(statusCode).json(data);
  }
}
