import { Logger } from '@nestjs/common';

export const loggerMessage = (logger: Logger, dataLogger: any) => {
  logger.log('============================');
  logger.warn(dataLogger);
  logger.log('============================');
};
