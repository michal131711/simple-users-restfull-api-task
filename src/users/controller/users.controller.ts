import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpException,
  HttpStatus,
  Post,
  Res,
  UseGuards,
} from '@nestjs/common';
import { UsersService } from '../services/users.service';
import { UserModel } from '../model/UserModel';
import {
  ApiBearerAuth,
  ApiBody,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';
import { SingleUserDto } from '../dto/singleUserDto';
import { BasicAuthGuard } from '../guards/basic-auth/basic-auth.guard';
import { CustomLogger } from '../../helpers/CustomLogger';

@ApiTags('UsersController')
@ApiBearerAuth('basic')
@Controller()
export class UsersController {
  private userServices: UsersService;
  private customLogger: CustomLogger;

  constructor(readonly userService: UsersService, readonly log: CustomLogger) {
    this.userServices = userService;
    this.customLogger = log;
  }

  @Get('users')
  @HttpCode(200)
  @ApiOkResponse({
    description: 'Users retrieved successfully.',
    type: UserModel,
  })
  @UseGuards(BasicAuthGuard)
  async getUsers(@Res() res): Promise<UserModel[]> {
    try {
      const getAllUsers = await this.userServices.getUsers();

      return await this.customLogger.sendDataResponse(res, HttpStatus.OK, {
        getAllUsers: getAllUsers,
      });
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.INTERNAL_SERVER_ERROR,
          error: 'Trouble in the server',
        },
        HttpStatus.INTERNAL_SERVER_ERROR,
        {
          cause: error,
        },
      );
    }
  }

  @Post('user/:id')
  @HttpCode(201)
  @ApiBody({ type: SingleUserDto })
  @ApiOkResponse({ description: 'User retrieved detail in success.' })
  @UseGuards(BasicAuthGuard)
  async getUser(
    @Body() req: SingleUserDto,
    @Res() res: any,
  ): Promise<UserModel> {
    try {
      const getUser = await this.userServices.getUser(req.id);

      return await this.customLogger.sendDataResponse(res, HttpStatus.OK, {
        getUser: getUser,
      });
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.INTERNAL_SERVER_ERROR,
          error: 'Trouble in the server',
        },
        HttpStatus.INTERNAL_SERVER_ERROR,
        {
          cause: error,
        },
      );
    }
  }
}
