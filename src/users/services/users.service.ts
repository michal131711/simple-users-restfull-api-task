import { Injectable } from '@nestjs/common';
import { UserModel } from '../model/UserModel';
import { HttpService } from '@nestjs/axios';
import { AxiosResponse } from 'axios';
import { lastValueFrom, Observable } from 'rxjs';

@Injectable()
export class UsersService {
  private getAllUsersLink = `https://jsonplaceholder.typicode.com/users`;

  constructor(private readonly httpService: HttpService) {}

  async getUsers(): Promise<UserModel[]> {
    return await this.responseData(this.getAllUsersLink);
  }

  async getUser(id: number): Promise<UserModel[]> {
    return await this.responseData(`${this.getAllUsersLink}/${id}`);
  }

  async responseData(link: string) {
    const response$: Observable<AxiosResponse<UserModel[]>> =
      this.httpService.get<UserModel[]>(link);

    const response: AxiosResponse<UserModel[]> = await lastValueFrom(response$);

    return response.data;
  }
}
