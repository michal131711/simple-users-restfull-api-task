import { Logger, Module } from '@nestjs/common';
import { UsersController } from './controller/users.controller';
import { UsersService } from './services/users.service';
import { HttpModule } from '@nestjs/axios';
import { CustomLogger } from '../helpers/CustomLogger';

@Module({
  controllers: [UsersController],
  imports: [HttpModule],
  providers: [UsersService, Logger, CustomLogger],
  exports: [UsersModule],
})
export class UsersModule {}
