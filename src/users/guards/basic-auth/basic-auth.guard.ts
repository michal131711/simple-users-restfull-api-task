import {
  CanActivate,
  ExecutionContext,
  Injectable,
  Logger,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { loggerMessage } from '../../../helpers/functions';

@Injectable()
export class BasicAuthGuard implements CanActivate {
  private logger: Logger;

  constructor(log: Logger) {
    this.logger = log;
  }

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    return this.validateRequest(request);
  }

  private validateRequest(request: any): boolean {
    const { headers } = request;
    const authHeader = headers.authorization;

    if (!authHeader || !authHeader.startsWith('Basic ')) {
      return false;
    }

    const encodedCredentials = authHeader.split(' ')[1];
    const decodedCredentials = Buffer.from(
      encodedCredentials,
      'base64',
    ).toString('utf-8');

    const username = process.env.HTTP_BASIC_USER;
    const password = process.env.HTTP_BASIC_PASS;

    loggerMessage(
      this.logger,
      `Request - endpoint signIn user in swagger: requestId: ${request.ip}`,
    );

    return decodedCredentials === `${username}:${password}`;
  }
}
