import { IUser } from '../interfaces/IUser';
import { Address } from '../base/Address';
import { ApiProperty } from '@nestjs/swagger';
import { CompanyDto } from '../dto/CompanyDto';

export class UserModel implements IUser {
  @ApiProperty({ type: Number, description: 'id for UserModel' })
  id: number;
  @ApiProperty({ type: String, description: 'name for UserModel' })
  name: string;
  @ApiProperty({ type: Address, description: 'address for UserModel' })
  address: Address;
  @ApiProperty({ type: CompanyDto, description: 'company for UserModel' })
  company: CompanyDto;
  @ApiProperty({ type: String, description: 'email for UserModel' })
  email: string;
  @ApiProperty({ type: String, description: 'phone for UserModel' })
  phone: string;
  @ApiProperty({ type: String, description: 'username for UserModel' })
  username: string;
  @ApiProperty({ type: String, description: 'website for UserModel' })
  website: string;
}
