import { ApiProperty } from '@nestjs/swagger';
import { IAddress } from '../interfaces/IAddress';
import { GeoDto } from '../dto/GeoDto';

export abstract class Address implements IAddress {
  @ApiProperty({ type: String, description: 'street for Address' })
  street: string;
  @ApiProperty({ type: String, description: 'suite for Address' })
  suite: string;
  @ApiProperty({ type: String, description: 'city for Address' })
  city: string;
  @ApiProperty({ type: String, description: 'zipcode for Address' })
  zipcode: string;
  @ApiProperty({ type: GeoDto, description: 'geo for Address' })
  geo: GeoDto;
}
