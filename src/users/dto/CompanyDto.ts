import { ICompany } from '../interfaces/ICompany';
import { ApiProperty } from '@nestjs/swagger';

export class CompanyDto implements ICompany {
  @ApiProperty({ type: String, description: 'bs for company' })
  bs: string;
  @ApiProperty({ type: String, description: 'catchPhrase for company' })
  catchPhrase: string;
  @ApiProperty({ type: String, description: 'name for company' })
  name: string;
}
