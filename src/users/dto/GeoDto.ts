import { IGeo } from '../interfaces/IGeo';
import { ApiProperty } from '@nestjs/swagger';

export class GeoDto implements IGeo {
  @ApiProperty({ type: String, description: 'lat for GeoDto' })
  lat: string;
  @ApiProperty({ type: String, description: 'lng for GeoDto' })
  lng: string;
}
