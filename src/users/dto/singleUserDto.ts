import { ApiProperty } from '@nestjs/swagger';

export class SingleUserDto {
  @ApiProperty({ type: Number, description: 'id for SingleUserDto' })
  id: number;
}
